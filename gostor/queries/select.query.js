
/**
     * Select queries (Always keep db Attribute (SELECT  , IF , NULL) in caps  except column or table name ! )
     * Note - 
     * 1 - Always keep space in start and end of the query.
     */

module.exports = {

    selectDailyMigrationsQueries: ' SELECT dm.id migrationId , dm.sequence  , dm.status from daily_migrations  ',

    selectUserBasciDetails: ' SELECT u.id userId , s.id storeId , u.name userName, a.role userRole,  u.username primaryMobile, e.email as userEmail ',

    selectAndCheckDuplicatMigrationQueries: ' SELECT COUNT(*) queryCount from daily_migrations dm  ',

};
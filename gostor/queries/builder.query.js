const { _queries } = require(`${_pathConfig_}`);

const CommonQuery = require(`${_queries}/common.query`);
const InsertQuery = require(`${_queries}/insert.query`);
const SelectQuery = require(`${_queries}/select.query`);
const JoinQuery = require(`${_queries}/join.query`);
const WhereQuery = require(`${_queries}/where.query`);



module.exports = {
    /**
     *  Final query builder
     */

    selectDailyMigrationsQuery: `${SelectQuery.selectDailyMigrationsQueries} ${CommonQuery.orderBy} dm.id ${CommonQuery.descWithLimit1} ;`,

    migrationQueryDuplicacyCheck: `${SelectQuery.selectAndCheckDuplicatMigrationQueries} ${WhereQuery.whereMigrationIn} ${CommonQuery.groupBy} dm.id  ;`,

    insertOrUpdateRefreshTokenQuery: `${InsertQuery.insertTokenQuery} ${CommonQuery.onDuplicatUpdateQuery} token = ? ;`,

    selectUserBasciDetailsQuery: `${SelectQuery.selectUserBasciDetails} ${JoinQuery.joinUserSellersEmailsActors} ${WhereQuery.whereUser} ;`
};

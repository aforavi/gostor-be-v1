
module.exports = {
    /**
     * Common queries (Always keep db Attribute (SELECT  , IF , NULL) in caps  except column or table name ! )
     * Note - 
     * 1 - Always keep space in start and end of the query.
     */

    onDuplicatUpdateQuery: ' ON DUPLICATE KEY UPDATE ',
    orderBy: ' ORDER BY ',
    descWithLimit1: ' DESC LIMIT 1 ',
    descWithLimit: ' DESC LIMIT ',
    in: ' IN ( ? ) ',
    groupBy: ' GROUP BY '
};

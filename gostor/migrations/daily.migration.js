
module.exports = {
    /**
     *  Daily migration queries
     */
    queries: {
        1: {
            query: `CREATE TABLE IF NOT EXISTS daily_migrations (
            id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
            query TEXT NOT NULL,
            type ENUM('CREATE' , 'UPDATE' , 'ALTER' , 'MODIFY') NOT NULLL,
            status ENUM('SUCCESS' , 'FAILED') NOT NULLL,
            code VARCHAR(255) UNIQUE NOT NULL,
            reason TEXT DEFAULT NULL,
            index BIGINT UNSIGNED NOT NULL,
            created_by VARCHAR(255) UNIQUE NOT NULL,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY(id)
            );`, // db query
            type: 'CREATE', // db type
            name: "createDailyMigrationsSchema", // a meaningfull name for query
            code: "QRY0000001", // A unique sequential code
            sequence: 1, // Query sequence for matching last ran query
            createdBy: 'Avishek Singh' // Name of the owner
        },
        2: {
            query: `CREATE TABLE IF NOT EXISTS auth_tokens (
            id bigint unsigned NOT NULL AUTO_INCREMENT,
            user_id bigint unsigned unique NOT NULL,
            token text NOT NULL,
            type enum('REFRESH_TOKEN') NOT NULL,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY(id),
            UNIQUE KEY uk_user_id_token(user_id, type),
            CONSTRAINT fk_auth_tokens_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
            );`,
            type: 'CREATE',
            name: "createAuthTokensSchema",
            code: "QRY0000002",
            sequence: 2,
            createdBy: 'Avishek Singh'
        }
    }
};

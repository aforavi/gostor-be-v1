/**
 * JWT class for Authentication.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports
const jwt = require('jsonwebtoken')
const lodash = require('lodash')

//Internal imports
const { _executors, _errors, _configs, _utils } = require(`${_pathConfig_}`);
const { tokenExpiration, refreshTokenExpiration } = require(`${_configs}/util.config`);
const CatchError = require(`${_errors}/catch.error`);
const Error500 = require(`${_errors}/500.error`);
const { jwtSecretKey, jwtRefreshSecretKey } = require(`${_configs}/env.config`);
const jwtExecutor = require(`${_executors}/jwt.executor`);
const UserUtils = require(`${_utils}/user.util`);

class JWT {

    formattedPayload(payload) {
        try {
            return lodash.pick(payload,
                [
                    'userName',
                    'userId',
                    'storeId',
                    'userRole',
                    'primaryMobile',
                    'userEmail'
                ])
        } catch (error) {
            throw new CatchError(error);
        }
    }

    async getPayload(primaryMobile) {
        try {
            const result = await UserUtils.getUserBasicDetails(primaryMobile);
            return this.formattedPayload(result[0]);
        } catch (error) {
            throw new CatchError(error);
        }
    }

    generateAccessToken(payload) {
        try {
            let accessToken = jwt.sign(payload, jwtSecretKey, { expiresIn: tokenExpiration });
            return accessToken;
        } catch (error) {
            throw new CatchError(error);
        }
    }

    async generateRefreshToken(payload) {
        try {
            if (!payload || !payload.userId) {
                throw new Error500(8);
            }
            let refreshToken = jwt.sign(payload, jwtRefreshSecretKey, { expiresIn: refreshTokenExpiration });
            const values = [payload.userId, refreshToken, refreshToken];
            await jwtExecutor.insertOrUpdateRefreshToken(values);
            const accessToken = this.generateAccessToken(payload);
            return { accessToken, refreshToken };
        } catch (error) {
            throw new CatchError(error);
        }
    }

    verifyAccessToken(accessToken) {
        try {
            const result = jwt.verify(accessToken, jwtSecretKey);
            return this.formattedPayload(result);
        } catch (error) {
            throw new CatchError(error);
        }
    }

    verifyRefreshToken(refreshToken) {
        try {
            const result = jwt.verify(refreshToken, jwtRefreshSecretKey);
            return this.formattedPayload(result);
        } catch (error) {
            throw new CatchError(error);
        }
    }
}


module.exports = new JWT()
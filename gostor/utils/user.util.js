/**
 * JWT class for Authentication.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

//Internal imports
const { _executors, _errors } = require(`${_pathConfig_}`);
const CatchError = require(`${_errors}/catch.error`);
const Error500 = require(`${_errors}/500.error`);
const UserExecutor = require(`${_executors}/user.executor`);

class UserUtil {

    async getUserBasicDetails(primaryMobile) {
        try {
            if (!primaryMobile) {
                throw new Error500(10);
            }
            const values = [primaryMobile];
            const result = await UserExecutor.userBasicDetails(values);
            return result;
        } catch (error) {
            throw new CatchError(error);
        }
    }
}


module.exports = new UserUtil()
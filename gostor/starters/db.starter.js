/**
 * Define Database connection
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// Third party imports
const { createPool } = require('mysql2');

// Internal imports
const { _configs, _errors } = require(`${_pathConfig_}`);
const StarterConfig = require(`${_configs}/starter.config`);
const CommonConfigs = require(`${_configs}/common.config`);
const Error500 = require(`${_errors}/500.error`);

/**
* Db pool
*/
let dbPools = null;

class DB {
    // Initialize your database pool
    init() {
        try {
            let dbConfig = CommonConfigs.isProd ? { ...StarterConfig.dbConfigs.common, ...StarterConfig.dbConfigs.prod } : CommonConfigs.isTest ? { ...StarterConfig.dbConfigs.common, ...StarterConfig.dbConfigs.test } :
                { ...StarterConfig.dbConfigs.common, ...StarterConfig.dbConfigs.local };
            const pool = createPool(dbConfig);
            const promisedPool = pool.promise();
            dbPools = { pool, promisedPool }
            return dbPools;
        } catch (error) {
            throw new Error500(2);
        }
    };

    geDbPools() {
        return dbPools;
    }
}

module.exports = new DB();

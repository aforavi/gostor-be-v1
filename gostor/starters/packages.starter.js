/**
 * Enables the Securities for app
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports
const cors = require('cors');
const compress = require('compression');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const { Application } = require('express');

// Internal imports
const { _starters } = require(`${_pathConfig_}`);
const Logger  = require(`${_starters}/log.starter`).init();


class Package {
    /**
    * @param {Application} app The app.
    */
    mountPackages(app) {

        Logger.info('Adding security @ app...');

        app.use(cors())
        app.use(compress()) // compressing
        /* Application wide configurations */
        app.enable('strict routing')
        app.enable('case sensitive routing')
        app.enable('trust proxy')
        app.set('etag', 'strong')
        /* Logger initialised for console and file streams. */
        // app.use(morgan('dev'))
        app.use(morgan('combined', { stream: Logger.stream }))
        app.use(cookieParser())
        /* Securing against known vulnerabilities. */
        app.use(helmet())
        app.use(bodyParser.urlencoded({
            limit: '50mb',
            extended: true,
            parameterLimit: 50000
        }))
        app.use(bodyParser.json({ limit: '50mb' }))

        return app;
    }
}

module.exports = new Package();

/**
 * Define Logger connection
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// Third party imports
const winston = require('winston');
const { Loggly } = require('winston-loggly-bulk');
const os = require('os');
const appRoot = require('app-root-path')

// Internal imports
const { _configs } = require(`${_pathConfig_}`);
const CommonConfigs = require(`${_configs}/common.config`);
const StarterConfig = require(`${_configs}/starter.config`);
const InternalErrorCodesConfig = require(`${_configs}/internalErrorCodes.config`);


/**
 * Log pool
 */
let Logger = null;

class LOG {

    // Initialize your database pool
    init() {
        try {
            // instantiate a new Winston Logger with the settings defined above
            if (CommonConfigs.isLocal) {
                Logger = winston.createLogger({
                    transports: [
                        new Loggly(this.getOptions().loggly),
                        new winston.transports.File(this.getOptions().file),
                        new winston.transports.Console(this.getOptions().console)
                    ],
                    exitOnError: true
                });
            } else {
                Logger = winston.createLogger({
                    transports: [
                        new winston.transports.File(this.getOptions().file),
                        new winston.transports.Console(this.getOptions().console)
                    ],
                    exitOnError: true
                });
            }

            // create a stream object with a 'write' function that will be used by `morgan`
            Logger.stream = {
                write: function (message) {
                    // use the 'info' log level so the output will be picked up by both transports (file and console)
                    if (CommonConfigs.isLocal) {
                        console.log(message)
                    }
                    // Logger.info(message);
                },
            };

            if (!Logger) {
                throw new Error(InternalErrorCodesConfig[4].code);
            }
            return Logger;
        } catch (error) {
            throw new Error(InternalErrorCodesConfig[5].code);
        }

    }

    consoleFormat = (info) => {
        if (CommonConfigs.isLocal) {
            console.log(info.message)
        }
        if (info.durationMs) {
            return `${info.timestamp} ${info.level}: ${info.message} |  durationMs: ${info.durationMs}`
        }
        return `${info.timestamp} ${info.level}: ${info.message}`
    }

    // define the custom settings for each transport (file, console)
    getOptions() {
        return {
            loggly: {
                token: StarterConfig.logglyToken,
                subdomain: "arzooo",
                tags: os.hostname() === StarterConfig.testHostName1 ? StarterConfig.logglyTestTag1 : StarterConfig.logglyProdTag1,
                json: true
            },
            file: {
                level: 'info',
                filename: `${appRoot}/logs/combined.log`,
                handleExceptions: true,
                maxsize: 20971520, // 20MB
                maxFiles: 5,
                format: winston.format.combine(
                    winston.format.timestamp(),
                    winston.format.splat(),
                    winston.format.json()
                )
            },
            console: {
                level: 'debug',
                handleExceptions: true,
                format: winston.format.combine(
                    winston.format.splat(),
                    winston.format.colorize(),
                    winston.format.timestamp(),
                    winston.format.align(),
                    winston.format.printf(this.consoleFormat)
                )
            },
        };
    }
}
;

module.exports = new LOG();
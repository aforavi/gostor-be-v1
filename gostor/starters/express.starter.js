/**
 * Creating Express server
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports
const express = require('express');

//Internal imports
const { _configs, _starters, _errors } = require(`${_pathConfig_}`);
const EnvConfig = require(`${_configs}/env.config`);
const RouteStarter = require(`${_starters}/routes.starter`);
const PackageStarter = require(`${_starters}/packages.starter`);
const CatchError = require(`${_errors}/catch.error`);
const Error500 = require(`${_errors}/500.error`);

class Server {
    /**
     * Create the express object
     */
    app = null;

    /**
     * Initializes the express server
     */
    constructor() {
        this.app = express();
        this.mountStarterPackages();
        this.mountRoutes();
    }

    /**
     * Mounts all the defined middlewares
     */
    mountStarterPackages() {
        try {
            PackageStarter.mountPackages(this.app);
        } catch (error) {
            throw new CatchError(error);
        }
    }

    /**
     * Mounts all the defined routes
     */
    mountRoutes() {
        try {
            RouteStarter.mountloadBalancerApi(this.app);
            RouteStarter.mountExternalApi(this.app);
            RouteStarter.mountInternalApi(this.app);
        } catch (error) {
            throw new CatchError(error);
        }
    }

    /**
     * Starts the express server
     */
    async init() {
        try {
            // Start the server on the specified port
            this.app.listen(EnvConfig.port, () => {
                return console.log(`Server :: Running @ '${EnvConfig.port}'`);
            });
        } catch (error) {
            throw new Error500(1);
        }
    }
}

/** Export the express module */
module.exports = new Server();

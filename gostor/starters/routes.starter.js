/**
 * Define Database connection
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

//External imports
const { Application } = require('express');

//Internal imports
const { _starters, _configs, _routers, _errors } = require(`${_pathConfig_}`);
const StarterConfig = require(`${_configs}/starter.config`);
const Logger = require(`${_starters}/log.starter`).init();
const ExternalRouter = require(`${_routers}/external.router`);
const InternalRouter = require(`${_routers}/internal.router`);
const CatchError = require(`${_errors}/catch.error`);

class Routes {
    /**
       * @param {Application} app The app.
       */
    mountExternalApi(app) {
        try {
            Logger.info('Routes :: Mounting External API Routes...');
            app.use(StarterConfig.externalApiPrefix, ExternalRouter);
        } catch (error) {
            throw new CatchError(error);
        }

    }
    /**
       * @param {Application} app The app.
       */
    mountInternalApi(app) {
        try {
            Logger.info('Routes :: Mounting Internal API Routes...');
            app.use(StarterConfig.internalApiPrefix, InternalRouter);
        } catch (error) {
            throw new CatchError(error);
        }
    }


    /**
       * @param {Application} app The app.
       */
    mountloadBalancerApi(app) {
        try {
            Logger.info('Routes :: Mounting load balancer API...');
            // Setup /health path for load balancer
            app.get('/health', function (req, res) {
                res.send('healthy')
            })
        }
        catch (error) {
            throw new CatchError(error);
        }
    }
}





module.exports = new Routes();

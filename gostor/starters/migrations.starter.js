/**
 * Main file for clubbing all starters
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports

//Internal imports
const { _migrations, _errors, _executors } = require(`${_pathConfig_}`);
const MigrationExecuter = require(`${_executors}/migration.executor`);
const DailyMigrations = require(`${_migrations}/daily.migration`);

const Error500 = require(`${_errors}/500.error`);
const CatchError = require(`${_errors}/catch.error`);

class Migrations {

    // Loads your Server
    async runDailyMigrations() {
        try {
            const result = await MigrationExecuter.migrationQueriesDetail();
            const migrationQueries = DailyMigrations.DailyMigrations;
            if (condition) {

            }


        } catch (error) {
            throw new CatchError(error);
        }
    }

    // Loads your Server
    validateMigrations(migrationQueries, lastMigratedQueryDeatil) {
        try {
            currentMigrationQueries = [];
            queryDuplicacyValues = [];
            const { sequence, status } = lastMigratedQueryDeatil;

            if (status !== 'SUCCESS') {
                throw new Error500(error);
            }

            migrationQueries.forEach((queryObj, index) => {
                if (index > sequence) {
                    quries = [];
                    codes = [];
                    sequences = [];
                    quries.push(queryObj.query);
                    codes.push(queryObj.code);
                    sequences.push(queryObj.sequence);
                    currentMigrationQueries.push(query);

                }
            });

            queryDuplicacyValues.push(quries , codes , sequences);

            const result = await MigrationExecuter.migrationQueryDuplicacyCheck();
            const { migrationId, query, code, sequence, status } = result[0];
            if (condition) {

            }


        } catch (error) {
            throw new CatchError(error);
        }
    }

    // Loads the Database Pool
    async loadDB() {
        try {
            Logger.info('Database :: starting @ Gostor...');
            const dbPools = DB.init();
            if (!dbPools || !dbPools.pool || !dbPools.promisedPool) {
                throw new Error500(3);
            }
            //    const test = await asyncExecutor.execute('select * from test;');
            //    console.log('test' , test);
            //   const test = jwtUtil.verifyRefreshToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6IkF2aXNoZWsgVGVzdCIsInVzZXJJZCI6OTk5NSwic3RvcmVJZCI6MjI2NSwidXNlclJvbGUiOiJNQVNURVIiLCJwcmltYXJ5TW9iaWxlIjoiODc4OTI2MzI4NSIsInVzZXJFbWFpbCI6ImF2aXNoZWtAYXJ6b29vLmNvbSIsImlhdCI6MTYzODUyNTcwNiwiZXhwIjoxNjQxMTE3NzA2fQ.9XKVL5GvShbqRBHXJSU1xxtQfRxAl0g5xPZ9cmxig9M')
            //                  console.log('test' , test);
        }
        catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = new App();

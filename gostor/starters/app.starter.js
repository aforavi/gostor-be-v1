/**
 * Main file for clubbing all starters
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports

//Internal imports
const { _starters, _errors, _executors, _utils } = require(`${_pathConfig_}`);
const ExpressStarter = require(`${_starters}/express.starter`);
const DB = require(`${_starters}/db.starter`);
const Logger = require(`${_starters}/log.starter`).init();
const Error500 = require(`${_errors}/500.error`);
const CatchError = require(`${_errors}/catch.error`);
// const asyncExecutor = require(`${_executors}/async.executor`);
const jwtUtil = require(`${_utils}/jwt.util`);

class App {

    // Loads your Server
    async loadServer() {
        try {
            Logger.info('Server :: starting @ Gostor...');
            await ExpressStarter.init();
        } catch (error) {
            throw new CatchError(error);
        }
    }

    // Loads the Database Pool
    async loadDB() {
        try {
            Logger.info('Database :: starting @ Gostor...');
            const dbPools = DB.init();
            if (!dbPools || !dbPools.pool || !dbPools.promisedPool) {
                throw new Error500(3);
            }
            //    const test = await asyncExecutor.execute('select * from test;');
            //    console.log('test' , test);
        //   const test = jwtUtil.verifyRefreshToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6IkF2aXNoZWsgVGVzdCIsInVzZXJJZCI6OTk5NSwic3RvcmVJZCI6MjI2NSwidXNlclJvbGUiOiJNQVNURVIiLCJwcmltYXJ5TW9iaWxlIjoiODc4OTI2MzI4NSIsInVzZXJFbWFpbCI6ImF2aXNoZWtAYXJ6b29vLmNvbSIsImlhdCI6MTYzODUyNTcwNiwiZXhwIjoxNjQxMTE3NzA2fQ.9XKVL5GvShbqRBHXJSU1xxtQfRxAl0g5xPZ9cmxig9M')
        //                  console.log('test' , test);
        }
        catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = new App();

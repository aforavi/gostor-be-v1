/**
 * Entry file for app
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports
const path = require('path');

// set root path
global._root_ = path.resolve();
global._pathConfig_ = `${_root_}/gostor/configs/path.config`;

// Internal imports
const { _starters } = require(`${_pathConfig_}`);
const appStarter = require(`${_starters}/app.starter`);


(async function startApp() {

     /**
          * Load the Database
          */
      await appStarter.loadDB();

     /**
      * Load the Server
      */
     await appStarter.loadServer();

})()


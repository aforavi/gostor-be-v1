/**
 * error code for externale reasons like npm /third party/ unkown error.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

module.exports = {
    1: {
        code: "ERREX000000",
        msg: "Unkown Error...",
        folder: "handlers",
        file: "error.handler.js",
        func: "unknownError",
        httpCode: 500,
        status: 'error'
    }
};

module.exports = {
    /**
     * Config for utils
     */

    // JWT configs
    tokenExpiration: '900s',
    // adminExipration : '900s',
    refreshTokenExpiration: '30d',
    // emailInterval : '7d'

};

const dotenv = require('dotenv');

const envFound = dotenv.config();
if (envFound.error) {
   // This error should crash whole process

   throw new Error("Couldn't find .env file!");
}

module.exports = {
   /**
    * server port
    */
   port: parseInt(process.env.npm_package_config_port || '4000', 10) || 4000,

   /**
    * Node Env identifier
    */
   nodeEnv: process.env.ENVIRONMENT,

   /**
      * AWS S3 access key
      */
   awsS3AccessKey: process.env.AWS_KEY,
   /**
    * AWS S3 secret key
    */
   awsS3secretKey: process.env.AWS_SECRET_KEY,

   /**
      * Redis host url
      */
   redisHost: process.env.REDIS_HOST,

   /**
   * Redis port
   */
   redisPort: process.env.REDIS_PORT,

   /**
      * Redis port
      */
   aeWebhookSecretKey: process.env.AE_WEBHOOK_SECRET_KEY,

   /**
      * Prod DB Configs
      */
   prodDbConfigs: {
      host: process.env.PROD_DB_HOST,
      database: process.env.PROD_DB_DATABASE,
      user: process.env.PROD_DB_USER,
      password: process.env.PROD_DB_PASSWORD
   },

   /**
      * Test DB Configs
      */
   testDbConfigs: {
      host: process.env.TEST_DB_HOST,
      database: process.env.TEST_DB_DATABASE,
      user: process.env.TEST_DB_USER,
      password: process.env.TEST_DB_PASSWORD
   },

   /**
      * Local DB Configs
      */
   localDbConfigs: {
      host: process.env.LOCAL_DB_HOST,
      database: process.env.LOCAL_DB_DATABASE,
      user: process.env.LOCAL_DB_USER,
      password: process.env.LOCAL_DB_PASSWORD
   },

   /**
    * Karix Access key
    */
   karixAccessKey: process.env.karixArzoooTrAccessKey,

   /**
   * Karix DLTID
   */
   karixDLTID: process.env.karixArzoooDLTID,

   /**
      * google client id
      */
   googleClientId: process.env.GOOGLE_CLIENT_ID,

   /**
      * google external client id
      */
   googleExternalClientId: process.env.GOOGLE_EXTERNAL_CLIENT_ID,

   /**
    * jwt secret key
    */
   jwtSecretKey: process.env.JWT_TOKEN_SECRET,

   /**
    * jwt refresh secret 
    */
   jwtRefreshSecretKey: process.env.JWT_REFRESH_SECRET,

   /**
    * Email secret key
    */
   emailSecretKey: process.env.EMAIL_SECRET,
   /**
    * Zoom api key
    */
   zoomApiKey: process.env.ZOOM_API_KEY,

   /**
   * Zoom secret key
   */
   zoomSecretKey: process.env.ZOOM_API_SECRET,

   /**
    * Capacity token
    */

   capacityToken: process.env.CAPPASITY_TOKEN

};


module.exports = {
    0: {
        code: "ERRIN000000",
        msg: "Unkown Error...",
        folder: "handlers",
        file: "error.handler.js",
        func: "loadServer",
        httpCode: 500,
        status: 'error'

    },
    1: {
        code: "ERRIN000001",
        msg: "Server is not loading...",
        folder: "starters",
        file: "app.starter.js",
        func: "loadServer",
        httpCode: 500,
        status: 'error'

    },
    2: {
        code: "ERRIN000002",
        msg: "Something wrong while connecting DB...",
        folder: "starters",
        file: "app.starter.js",
        func: "loadDB",
        httpCode: 500,
        status: 'error'

    },
    3: {
        code: "ERRIN000003",
        msg: "DB pool is missing...",
        folder: "starters",
        file: "app.starter.js",
        func: "loadDB",
        httpCode: 500,
        status: 'error'

    },
    4: {
        code: "ERRIN000004",
        msg: "Logger pool is missing...",
        folder: "starters",
        file: "log.starter.js",
        func: "init",
        httpCode: 500,
        status: 'error'

    },
    5: {
        code: "ERRIN000005",
        msg: "Something wrong while connecting Logger...",
        folder: "starters",
        file: "log.starter.js",
        func: "init",
        httpCode: 500,
        status: 'error'

    },
    6: {
        code: "ERRIN000006",
        msg: "Promised db pool is missing...",
        folder: "executers",
        file: "async.executer.js",
        func: "execute",
        httpCode: 500,
        status: 'error'

    },
    7: {
        code: "ERRIN000007",
        msg: "Query is empty...",
        folder: "executers",
        file: "async.executer.js",
        func: "execute",
        httpCode: 500,
        status: 'error'

    },
    8: {
        code: "ERRIN000008",
        msg: "Wrong payload...",
        folder: "utils",
        file: "jwt.util.js",
        func: "generateRefreshToken",
        httpCode: 500,
        status: 'error'

    },
    9: {
        code: "ERRIN000009",
        msg: "Wrong token...",
        folder: "middlewares",
        file: "auth.middleware.js",
        func: "authenticateAccessToken",
        httpCode: 401,
        status: 'error'
    },
    10: {
        code: "ERRIN000010",
        msg: "Primary mobile missing...",
        folder: "utils",
        file: "user.util.js",
        func: "getUserBasicDetails",
        httpCode: 500,
        status: 'error'
    },
    11: {
        code: "ERRIN000011",
        msg: "Validation error...",
        folder: "validators",
        file: "joi.validator.js",
        func: "init",
        httpCode: 500,
        status: 'error'

    }
};

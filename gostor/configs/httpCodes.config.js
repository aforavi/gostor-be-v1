
module.exports = {
    OK: 200,
    BAD_REQUEST: 400,
    UNAUTHOROZED: 401,
    NOT_FOUND: 404,
    INTERNAL_SERVER: 500
};

const { _configs } = require(`${_pathConfig_}`);
const EnvConfig = require(`${_configs}/env.config`);

module.exports = {
    /** 
        * DB Configs
        */
    dbConfigs: {
        prod: EnvConfig.prodDbConfigs,
        test: EnvConfig.testDbConfigs,
        local: EnvConfig.localDbConfigs,
        common: {
            connectionLimit: 30,
            charset: "utf8_general_ci",
            supportBigNumbers: true,
            bigNumberStrings: false,
            connectTimeout: 3600000,
            // acquireTimeout: 3600000,
            multipleStatements: true,
            waitForConnections: true,
            queueLimit: 0,
            timezone: "+00:00",
            debug: false
        }
    },
    /**
     * Db pool
     */

    logglyToken: '55b64aca-edbb-4dbd-a23e-b69df25265d7',

    /**
     * host name
     */

    testHostName1: 'test-1-be-gostor',

    /**
    * loggly test1 tag
    */

    logglyTestTag1: 'test-1-gostor',

    /**
    * loggly prod tag
    */

    logglyProdTag1: 'prod-gostor',

    /**
    * External api prefix
    */

    externalApiPrefix: '/luna',

    /**
   * inernal api prefix
   */

    internalApiPrefix: '/terra'


};

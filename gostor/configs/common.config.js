const { _configs } = require(`${_pathConfig_}`);
const EnvConfig = require(`${_configs}/env.config`);

module.exports = {
    /**
     * prod server identifier
     */
    isProd: EnvConfig.nodeEnv === 'prod' || false,

    /**
    * test server identifier
    */
    isTest: EnvConfig.nodeEnv === 'test' || false,

    /**
     * local server identifier
     */
    isLocal: EnvConfig.nodeEnv === 'local' || false,
    /**
     * Db pool
     */
};

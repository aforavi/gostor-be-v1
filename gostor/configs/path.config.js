module.exports = {
    /**
     * path shorting
     */
    "_assets": `${_root_}/gostor/assets`,
    "_configs": `${_root_}/gostor/configs`,
    "_assetes": `${_root_}/gostor/assetes`,
    "_connectors": `${_root_}/gostor/connectors`,
    "_controllers": `${_root_}/gostor/controllers`,
    "_queries": `${_root_}/gostor/queries`,
    "_errors": `${_root_}/gostor/errors`,
    "_executors": `${_root_}/gostor/executors`,
    "_externals": `${_root_}/gostor/externals`,
    "_helpers": `${_root_}/gostor/helpers`,
    "_middlewares": `${_root_}/gostor/middlewares`,
    "_migrations": `${_root_}/gostor/migrations`,
    "_notifiers": `${_root_}/gostor/notifiers`,
    "_public": `${_root_}/gostor/public`,
    "_routers": `${_root_}/gostor/routers`,
    "_services": `${_root_}/gostor/services`,
    "_starters": `${_root_}/gostor/starters`,
    "_utils": `${_root_}/gostor/utils`,
    "_validators": `${_root_}/gostor/validators`,
    "_views": `${_root_}/gostor/views`,
    "_handlers": `${_root_}/gostor/handlers`
};

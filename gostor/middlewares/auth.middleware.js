/**
 * Authentication middleware for api .
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// Internal imports

const { _utils, _errors, _handlers, _validators } = require(`${_pathConfig_}`);
const JwtUtils = require(`${_utils}/jwt.util`);
const Error401 = require(`${_errors}/401.error`);
const ErrorHandler = require(`${_handlers}/error.handler`);
const AuthValidator = require(`${_validators}/auth.validator`);


class AuthMiddleware {

    authenticateAccessToken(req, res, next) {
        try {
            const authHeader = req.headers['authorization'];
            const token = authHeader && authHeader.split(' ')[1];
            if (!token) throw new Error401(9);
            JwtUtils.verifyAccessToken(token);
            next();
        } catch (error) {
            return new ErrorHandler(res, error);
        }
    }

    async validateGetAccessToken(req, res, next) {
        try {
            await AuthValidator.validateAccessToken(req.query);
            next();
        } catch (error) {
            return new ErrorHandler(res, error);
        }
    }

    async validateGetRefreshToken(req, res, next) {
        try {
            await AuthValidator.validateRefreshToken(req.query);
            next();
        } catch (error) {
            return new ErrorHandler(res, error);
        }
    }
}

module.exports = new AuthMiddleware()
/**
 * Validatior class for auth module.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */


const { _errors } = require(`${_pathConfig_}`);
const CatchError = require(`${_errors}/catch.error`);
const Error500 = require(`${_errors}/500.error`);

class Validator {

    async init(params, schema) {
        try {
            const result = await schema.validateAsync(params);
            return result
        } catch (error) {
            throw new CatchError(error);
        }
    }

}

module.exports = new Validator()
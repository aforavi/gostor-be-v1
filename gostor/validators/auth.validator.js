
/**
 * Validatior class for auth module.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

//Extenal Imports
const joi = require('joi');

//Internal Imports
const { _validators, _errors } = require(`${_pathConfig_}`);
const Validator = require(`${_validators}/joi.validator`);
const CatchError = require(`${_errors}/catch.error`);

class AuthValidator {

    async validateAccessToken(params) {
        try {
            const schema = joi.object().keys({
                refreshToken: joi.string().disallow('', ' ').required()
            })
            return await Validator.init(params, schema)
        } catch (error) {
            throw new CatchError(error);
        }
    }

    async validateRefreshToken(params) {
        try {
            const schema = joi.object().keys({
                primaryMobile: joi.number().disallow('', ' ').required()
            })
            return await Validator.init(params, schema)
        } catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = new AuthValidator()
/**
 * Async executor class to make migrations db calls.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _executors, _errors, _queries } = require(`${_pathConfig_}`);
const DbExecutor = require(`${_executors}/db.executor`);
const { selectDailyMigrationsQuery, migrationQueryDuplicacyCheck } = require(`${_queries}/builder.query`);
const CatchError = require(`${_errors}/catch.error`);


class MigrationExecutor {

    async migrationQueriesDetail(values = []) {
        try {
            const result = await DbExecutor.execute(selectDailyMigrationsQuery, values);
            return result;
        } catch (error) {
            throw new CatchError(error);
        }
    }

    async migrationQueryDuplicacyCheck(values = []) {
        try {
            const result = await DbExecutor.execute(selectDailyMigrationsQuery, values);
            return result;
        } catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = new MigrationExecutor()
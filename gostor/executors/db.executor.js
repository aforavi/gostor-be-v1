/**
 * Async executor class for to make db calls.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _starters, _errors } = require(`${_pathConfig_}`);
const DB = require(`${_starters}/db.starter`);
const Error500 = require(`${_errors}/500.error`);
const CatchError = require(`${_errors}/catch.error`);

class DbExecutor {

    async execute(sql = '', values = []) {
        try {
            const dbPools = DB.geDbPools();
            if (!dbPools || !dbPools.promisedPool) {
                throw new Error500(6);
            }
            if (!sql) {
                throw new Error500(7);
            }
            const [rows, fields] = await dbPools.promisedPool.query({ sql, values })
            return rows;
        } catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = new DbExecutor()
/**
 * Async executor class to make jwt db calls.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _executors, _errors, _queries } = require(`${_pathConfig_}`);
const DbExecutor = require(`${_executors}/db.executor`);
const { insertOrUpdateRefreshTokenQuery } = require(`${_queries}/builder.query`);
const CatchError = require(`${_errors}/catch.error`);


class JwtExecutor {

    async insertOrUpdateRefreshToken(values = []) {
        try {
            const result = await DbExecutor.execute(insertOrUpdateRefreshTokenQuery, values);
            return result;
        } catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = new JwtExecutor()
/**
 * error class for bad requests.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _errors, _configs } = require(`${_pathConfig_}`);
const HttpCodesConfig = require(`${_configs}/httpCodes.config`);
const InternalErrorCodesConfig = require(`${_configs}/internalErrorCodes.config`);
const CustomError = require(`${_errors}/custom.error`);

class Error404 extends CustomError {
    constructor(
        errNo,
        httpCode = HttpCodesConfig.NOT_FOUND,
        msg = InternalErrorCodesConfig[errNo]['msg'],
    ) {
        super(InternalErrorCodesConfig[errNo]['code'], httpCode, msg, errNo)
        this.logError(InternalErrorCodesConfig[errNo]['code'])
    }

    logError(code) {
        Logger.error(`${code}`)
    }
}

module.exports = Error404
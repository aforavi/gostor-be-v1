
/**
 * Customizing node error class.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

class CustomError extends Error {
    constructor(code, httpCode, msg, errNo) {
        super(msg)

        Object.setPrototypeOf(this, new.target.prototype)
        this.code = code
        this.httpCode = httpCode
        this.errNo = errNo
        Error.captureStackTrace(this)
    }
}

module.exports = CustomError
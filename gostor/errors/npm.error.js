/**
 * error class for bad requests.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _starters } = require(`${_pathConfig_}`);
const Logger = require(`${_starters}/log.starter`).init();

class NpmError {
    constructor(
        error
    ) {
        this.init(error);
    }

    init(err) {
        this.logError(`NpmError: ${err.name} | ${err.message} | ${err.stack}`);
        throw new Error(err);
    }

    logError(code) {
        Logger.error(`${code}`)
    }
}

module.exports = NpmError
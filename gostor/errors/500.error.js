/**
 * error class for bad requests.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _errors, _configs, _starters } = require(`${_pathConfig_}`);
const HttpCodesConfig = require(`${_configs}/httpCodes.config`);
const InternalErrorCodesConfig = require(`${_configs}/internalErrorCodes.config`);
const CustomError = require(`${_errors}/custom.error`);
const Logger = require(`${_starters}/log.starter`).init();

class Error500 extends CustomError {
    constructor(
        errNo,
        httpCode = HttpCodesConfig.INTERNAL_SERVER,
        msg = InternalErrorCodesConfig[errNo]['msg'],
    ) {
        super(InternalErrorCodesConfig[errNo]['code'], httpCode, msg, errNo);
        this.logError(InternalErrorCodesConfig[errNo]['code'])
    }

    logError(code) {
        Logger.error(`${code}`)
    }
}

module.exports = Error500
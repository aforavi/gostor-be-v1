/**
 * error class for bad requests.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _errors, _configs, _starters } = require(`${_pathConfig_}`);
const InternalErrorCodesConfig = require(`${_configs}/internalErrorCodes.config`);
const Error400 = require(`${_errors}/400.error`);
const Error401 = require(`${_errors}/401.error`);
const Error404 = require(`${_errors}/404.error`);
const Error500 = require(`${_errors}/500.error`);
const Logger = require(`${_starters}/log.starter`).init();

class CatchError {
    constructor(
        error
    ) {
        this.init(error);
    }

    init(err) {
        if (err && err.errNo && InternalErrorCodesConfig[err.errNo] && err.httpCode) {
            if (err.httpCode == 400) {
                throw new Error400(err.errNo);
            } else if (err.httpCode == 401) {
                throw new Error401(err.errNo);
            } else if (err.httpCode == 404) {
                throw new Error404(err.errNo);
            } else {
                throw new Error500(err.errNo);
            }
        } else {
            this.logError(`CatchError: ${err.name} | ${err.message} | ${err.stack}`);
            throw new Error(err);
        }
    }

    logError(code) {
        Logger.error(`${code}`)
    }
}

module.exports = CatchError
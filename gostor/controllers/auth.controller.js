/**
 * Authentication controllers for api .
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// Internal imports
const { _handlers, _utils } = require(`${_pathConfig_}`);
const SuccessHandler = require(`${_handlers}/success.handler`);
const ErrorHandler = require(`${_handlers}/error.handler`);
const JwtUtils = require(`${_utils}/jwt.util`);

class AuthController {

    getAccessToken(req, res) {
        try {
            const { refreshToken } = req.query;
            const payload = JwtUtils.verifyRefreshToken(refreshToken);
            const accessToken = JwtUtils.generateAccessToken(payload);
            return new SuccessHandler(res, accessToken, 'accessToken');
        } catch (error) {
            return new ErrorHandler(res, error);
        }
    }

    async getRefreshToken(req, res) {
        try {
            const { primaryMobile } = req.query;
            const payload = await JwtUtils.getPayload(primaryMobile);
            const refreshToken = await JwtUtils.generateRefreshToken(payload);
            return new SuccessHandler(res, refreshToken);
        } catch (error) {
            return new ErrorHandler(res, error);
        }
    }
}

module.exports = new AuthController()
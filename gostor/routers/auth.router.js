// External imports
const router = require('express').Router();

//Internal imports
const { _middlewares, _controllers } = require(`${_pathConfig_}`);
const AuthMiddleware = require(`${_middlewares}/auth.middleware`);
const AuthController = require(`${_controllers}/auth.controller`);

/**
 * Api to get the access token .
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

router.get('/accessToken', AuthMiddleware.validateGetAccessToken, AuthController.getAccessToken);

/**
 * Api to get the refresh token .
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

router.get('/refreshToken', AuthMiddleware.validateGetRefreshToken, AuthController.getRefreshToken);

module.exports = router;
/**
 * Define all your Internal API routes
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

// External imports
const router = require('express').Router();

//Internal imports
// External imports
const { _routers } = require(`${_pathConfig_}`);
const AuthRouter = require(`${_routers}/auth.router`);

/**
 * Authentication module apis.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

router.use('/auth', AuthRouter);



module.exports = router;

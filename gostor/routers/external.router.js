/**
 * Define all your Internal API routes
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { Router } = require('express');

const router = Router();


module.exports = router;

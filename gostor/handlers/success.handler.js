/**
 * Common success handler for sending response.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _errors } = require(`${_pathConfig_}`);
const CatchError = require(`${_errors}/catch.error`);

class SuccessHandler {

    constructor(
        res,
        data,
        clientKeyName
    ) {
        return this.init(res, data, clientKeyName);
    }

    init(res, data, clientKeyName) {
        const response = {
            status: 'success',
            data: {}
        }
        try {
            if (!clientKeyName || (typeof clientKeyName !== 'string')) {
                response.data = data;
            } else {
                response.data[clientKeyName] = data;
            }
            return res.status(200).send(response);
        } catch (error) {
            throw new CatchError(error);
        }
    }
}

module.exports = SuccessHandler
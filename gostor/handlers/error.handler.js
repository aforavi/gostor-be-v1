/**
 * Common error handler for sending response.
 *
 * @author Avishek Singh <avishek@arzooo.com>
 */

const { _starters, _configs } = require(`${_pathConfig_}`);
const Logger = require(`${_starters}/log.starter`).init();
const InternalErrorCodesConfig = require(`${_configs}/internalErrorCodes.config`);
const ExternalErrorCodesConfig = require(`${_configs}/externalErrorCodes.config`);

class ErrorHandler {

    constructor(
        res,
        error
    ) {
        return this.init(res, error);
    }

    init(res, error) {
        if (error && error.errNo && error.code) {
            return this.knownError(res, error);
        } else {
            return this.unknownError(res, error);
        }
    }

    knownError(res, error) {
        Logger.error(`ErrorHandler:knownError: ${error.code} | ${error.name} | ${error.message} | ${error.stack}`);
        const { httpCode, status, code, msg } = InternalErrorCodesConfig[error.errNo];
        const err = {
            type: error && error.name ? error.name : '',
            message: error && error.message ? error.message : '',
        }
        return res.status(httpCode).send({ status, code, msg, err });
    }

    unknownError(res, error) {
        const { httpCode, status, code, msg } = ExternalErrorCodesConfig[1];
        Logger.error(`ErrorHandler:unknownError: ${code} | ${error.name} | ${error.message} | ${error.stack}`);
        const err = {
            type: error && error.name ? error.name : '',
            message: error && error.message ? error.message : '',
        }
        return res.status(httpCode).send({ status, code, msg, err });
    }
}

module.exports = ErrorHandler;